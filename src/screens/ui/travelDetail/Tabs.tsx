import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import TravelTypeTab from './TravelTypeTab';
import GetIcon from '../../components/GetIcon';
import { icon, isSfOrIon, isSfOrMaterial } from '../../../util/icon';

interface Props {
  isFullTabView?: boolean;
}

const Tabs: React.FC<Props> = ({ isFullTabView }) => {
  const desktopTabStyle = !isFullTabView && { padding: 8 };
  const expandTab = !isFullTabView && { flexGrow: 1 };
  const toShowOrNot = isFullTabView ? 'flex' : 'none';

  return (
    <View style={styles.travelTypeTabsContainer}>
      <ScrollView
        style={{ margin: isFullTabView ? 8 : 0 }}
        contentContainerStyle={{ flexGrow: 1 }}
        horizontal
        showsHorizontalScrollIndicator={false}
        alwaysBounceHorizontal={false}
      >
        <View style={expandTab}>
          <TravelTypeTab
            style={desktopTabStyle}
            title="One - Way"
            icon={
              <GetIcon
                name={icon.arrow_double}
                color="rgb(82, 176, 167)"
                size={18}
                type={isSfOrMaterial}
              />
            }
            isActive
          />
        </View>
        <View style={expandTab}>
          <TravelTypeTab
            style={desktopTabStyle}
            title="Round Trip"
            icon={
              <GetIcon
                name={icon.arrow_round}
                color="grey"
                size={18}
                type={isSfOrIon}
              />
            }
          />
        </View>
        <View style={[expandTab, { display: toShowOrNot }]}>
          <TravelTypeTab
            style={desktopTabStyle}
            title="Package"
            icon={
              <GetIcon
                name={icon.wallet}
                color="grey"
                size={18}
                type={isSfOrIon}
              />
            }
            // icon={<Ionicons name="wallet-outline" color="grey" size={18} />}
          />
        </View>
      </ScrollView>
      <Text style={[styles.knowMoreText, { display: toShowOrNot }]}>
        Know more about travel destination?{' '}
        <Text style={{ color: 'rgb(82, 176, 167)', fontWeight: '500' }}>
          Explore map{' '}
          <GetIcon
            name={icon.globe}
            color="rgb(82, 176, 167)"
            size={14}
            type={isSfOrIon}
          />
        </Text>
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  travelTypeTabsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap-reverse',
    alignItems: 'center',
  },
  knowMoreText: {
    color: 'grey',
    margin: 8,
    flexShrink: 1,
  },
});

export default Tabs;
