import React from 'react';
import {
  Pressable,
  StyleProp,
  StyleSheet,
  Text,
  View,
  ViewStyle,
} from 'react-native';

interface Props {
  style?: StyleProp<ViewStyle>;
  title: string;
  icon: JSX.Element;
  isActive?: boolean;
}

const TravelTypeTab: React.FC<Props> = ({ style, title, icon, isActive }) => {
  return (
    <Pressable>
      <View style={[styles.container, style]}>
        {icon}
        <Text style={[styles.title, isActive && styles.titleActive]}>
          {title}
        </Text>
      </View>
      <View
        style={[styles.tabIndicator, isActive && styles.tabIndicatorActive]}
      />
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 16,
  },
  title: {
    color: 'grey',
    fontSize: 18,
    fontWeight: '500',
    marginStart: 8,
  },
  titleActive: { color: 'rgb(82, 176, 167)', fontWeight: 'bold' },
  tabIndicator: {
    height: 3,
    width: '100%',
    borderRadius: 6,
    margin: 3,
  },
  tabIndicatorActive: {
    backgroundColor: 'rgb(82, 176, 167)',
    shadowColor: 'rgb(82, 176, 167)',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.6,
    shadowRadius: 4,
  },
});

export default TravelTypeTab;
