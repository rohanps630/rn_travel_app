import React, { useEffect, useState } from 'react';
import { Image, Pressable, StyleSheet, Text, View } from 'react-native';
import { fStyles } from './styles';
import GetIcon from '../../components/GetIcon';
import { formatTime } from '../../../util/date';
import { AppImages } from '../../../assets';
import { FlightDetail } from '../../../types';

interface FlightItemProps {
  info: FlightDetail;
  onClick?: () => void;
  // isListItem?: boolean;
}

const FlightItem: React.FC<FlightItemProps> = ({
  info,
  onClick,
  // isListItem = true,
}) => {
  // const window = useWindowDimensions();
  const [logoWidth, setLogoWidth] = useState(0);

  const source = info.displayData.source;
  const destination = info.displayData.destination;
  const airline = info.displayData.airlines[0];

  // Total grid columns for a particular window width, in grid view layout
  // const gridListCol = window.width > 1200 ? 3 : window.width > 768 ? 2 : 1;

  useEffect(() => {
    // Logo has alignment issues, so getting original size here
    airline.logo &&
      Image.getSize(
        airline.logo,
        (w, h) => setLogoWidth((w / h) * 20),
        _err => setLogoWidth(120),
      );
  }, [airline.logo]);

  return (
    <Pressable
      style={({ pressed }) => [
        styles.container,
        { opacity: pressed ? 0.6 : 1 },
        // Sets the item width so that it fits the grid layout as window resizes
        /* isListItem && {
          maxWidth: (window.width - 16 * (gridListCol + 1)) / gridListCol,
        }, */
      ]}
      disabled={!onClick}
      onPress={onClick}
    >
      <View style={styles.headerRow}>
        {!airline.logo ? (
          <Text style={styles.airlineName}>
            {airline.airlineName}, {airline.airlineCode}
          </Text>
        ) : (
          <Image
            style={[styles.airlineLogo, { width: logoWidth }]}
            source={{ uri: airline.logo }}
            resizeMode="cover"
          />
        )}
        <View
          style={[
            styles.filterFlag,
            {
              backgroundColor:
                info.filterFlag === 'seller'
                  ? 'rgba(96, 140, 195, 0.2)'
                  : 'rgba(82, 176, 167, 0.2)',
              display: info.filterFlag ? 'flex' : 'none',
            },
          ]}
        >
          <Text
            style={{
              fontSize: 12,
              fontWeight: 'bold',
              color:
                info.filterFlag === 'seller'
                  ? 'rgb(96, 140, 195)'
                  : 'rgb(82, 176, 167)',
            }}
          >
            {info.filterFlag === 'seller' ? 'Best Seller' : 'Recommended'}
          </Text>
        </View>
      </View>
      <View style={{ paddingHorizontal: 16 }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text style={fStyles.infoTextCorner}>
            {source.airport.cityName}, {source.airport.cityCode}
          </Text>
          <Text style={fStyles.centerText}>
            {info.displayData.totalDuration}
          </Text>
          <Text style={[fStyles.infoTextCorner, { textAlign: 'right' }]}>
            {destination.airport.cityName}, {destination.airport.cityCode}
          </Text>
        </View>
        <View style={styles.middleRow}>
          <Text style={fStyles.middleRowTextCorner}>
            {source.airport.airportCode}
          </Text>
          <View style={styles.flightVisualContainer}>
            <View style={fStyles.viewCircleBorder} />
            <View style={styles.flightDashRowContainer}>
              <View style={styles.dashedLine} />

              <View style={fStyles.flightIconContainer}>
                <GetIcon
                  style={fStyles.flightIcon}
                  name="airplane"
                  color="rgb(82, 176, 167)"
                  size={24}
                  type="ion"
                />
              </View>
            </View>
            <View style={fStyles.viewCircleBg} />
          </View>
          <Text style={[fStyles.middleRowTextCorner, { textAlign: 'right' }]}>
            {destination.airport.airportCode}
          </Text>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text style={fStyles.infoTextCorner}>
            {formatTime(source.depTime)}
          </Text>
          <Text style={fStyles.centerText}>{info.displayData.stopInfo}</Text>
          <Text style={[fStyles.infoTextCorner, { textAlign: 'right' }]}>
            {formatTime(destination.arrTime)}
          </Text>
        </View>
      </View>
      <View style={styles.footerClipperContainer}>
        <View style={[styles.cornerViewClipper, { right: 20 }]} />
        <View style={[{ flexGrow: 1 }, styles.dashedLine]} />
        <View style={[styles.cornerViewClipper, { left: 20 }]} />
      </View>
      <View style={styles.footerRow}>
        {/* {airline.flightNumber ? (
          <Text style={{ fontSize: 16, fontWeight: '400' }}>
            <Text style={{ fontWeight: 'bold' }}>Flight Number:</Text>{' '}
            {airline.flightNumber}
          </Text>
        ) : ( */}
        <View style={{ flexDirection: 'row', gap: 4 }}>
          <Image
            style={{ width: 18, height: 18 }}
            source={AppImages.business_icon}
            resizeMode="contain"
          />
          <Text style={{ fontSize: 16, fontWeight: '700' }}>
            Business Class
          </Text>
        </View>
        {/* )} */}
        <Text style={styles.flightPrice}>
          ₹{info.fare}{' '}
          {info.originalFare && (
            <Text style={fStyles.originalPrice}>₹{info.originalFare}</Text>
          )}
        </Text>
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'white',
    borderRadius: 16,
    margin: 8,
    overflow: 'hidden',
  },
  headerRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 16,
    marginBottom: 24,
  },
  filterFlag: {
    borderRadius: 99,
    padding: 4,
    paddingHorizontal: 12,
  },
  airlineLogo: {
    maxWidth: 120,
    height: 20,
  },
  airlineName: {
    color: 'rgb(82, 176, 167)',
    fontWeight: 'bold',
  },
  middleRow: {
    flexDirection: 'row',
    marginVertical: 12,
  },
  footerRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 16,
    paddingTop: 0,
  },
  flightPrice: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'rgb(82, 176, 167)',
  },
  flightVisualContainer: {
    flexGrow: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  flightDashRowContainer: {
    minHeight: 28,
    flexGrow: 1,
    justifyContent: 'center',
    marginHorizontal: 8,
  },
  dashedLine: {
    borderWidth: 1,
    borderColor: 'rgb(231, 236, 243)',
    borderStyle: 'dashed',
  },
  footerClipperContainer: {
    minHeight: 30,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
  },
  cornerViewClipper: {
    height: 30,
    width: 30,
    borderRadius: 15,
    backgroundColor: 'rgb(245, 247, 250)',
  },
});

export default FlightItem;
