import React, { useRef, useState } from 'react';
import {
  Animated,
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  Linking,
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { useSelector } from 'react-redux';
import { EdgeInsets, useSafeAreaInsets } from 'react-native-safe-area-context';
import { useNavigation } from '@react-navigation/native';
import {
  AppPreviewCard,
  FeatureAccordian,
  Header,
  MenuModal,
} from './components';
import TravelDetails from '../ui/travelDetail/TravelDetails';
import TravelDetailsDesktop from '../ui/travelDetail/TravelDetails.desktop';
import FlightItem from '../ui/flightItem/FlightItem';
import FlightItemDesktop from '../ui/flightItem/FlightItem.desktop';
import Button from '../components/Button';
import GetIcon from '../components/GetIcon';
import flightsList from '../../model/flights_list.json';
import { icon, isSfOrIon } from '../../util/icon';
import { SOCIALS } from '../../model/data';
import { RootState } from '../../store';
import { Config } from '../../config';
import { AppImages } from '../../assets';
import { WelcomeDeskNavProp } from '../../Routes';

// Just a custom rating view for now, not much usefull and customizable for now
const ratingView = () => (
  <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
    <GetIcon name="star" color="#FFC107" size={10} />
    <GetIcon name="star" color="#FFC107" size={10} />
    <GetIcon name="star" color="#FFC107" size={10} />
    <GetIcon name="star" color="#FFC107" size={10} />
    <GetIcon
      style={{ width: 5, marginEnd: 5, overflow: 'hidden' }}
      name="star"
      color="#FFC107"
      size={10}
    />
  </View>
);

const socialLinks = (iconName: string, color: string, url: string) => (
  <TouchableOpacity key={iconName} onPress={() => Linking.openURL(url)}>
    <GetIcon name={iconName} size={24} color={color} type="ion" />
  </TouchableOpacity>
);

const COLLAB_AIRLINES = [
  AppImages.airlines.singapore,
  AppImages.airlines.american,
  AppImages.airlines.delta,
  AppImages.airlines.british,
];

const LandingScene = () => {
  const insets = useSafeAreaInsets();
  const navigation = useNavigation<WelcomeDeskNavProp>();
  const travelState = useSelector((state: RootState) => state.travel_request);

  const [screenWidth, setScreenWidth] = useState(0);
  const [toggleMenu, setToggleMenu] = useState(false);

  const scrollRef = useRef<ScrollView>(null);
  const scrollOffset = useRef(new Animated.Value(0)).current;
  const travelInputsPosY = useRef(0);

  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor="rgb(23, 27, 34)" />
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Config.isIos ? 'padding' : undefined}
      >
        <ScrollView
          style={{ backgroundColor: 'white' }}
          onLayout={event => {
            const layoutWidth = event.nativeEvent.layout.width;
            setScreenWidth(layoutWidth);
            layoutWidth >= 768 && setToggleMenu(false);
          }}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: scrollOffset } } }],
            { useNativeDriver: false },
          )}
          scrollEventThrottle={16}
          ref={scrollRef}
        >
          <ImageBackground
            style={[styles.headerContainer, { paddingTop: insets.top }]}
            // @ts-expect-error
            imageStyle={pStyles.headerImageBg}
            source={AppImages.dotted_world_map}
            resizeMode="contain"
          >
            <View
              style={[
                styles.headerContentContainer,
                safeAreaStyle(insets).paddingH,
              ]}
            >
              <Header {...{ screenWidth }} onMenuToggle={setToggleMenu} />
              <View style={styles.headerContentView}>
                <View style={{ flex: 1 }}>
                  <Text style={styles.headerTitle}>
                    Book Your Flight Tickets in Easy Way
                  </Text>
                  <Text style={styles.headerSubtitle}>
                    We help you to find flights easily and efficiently. we
                    provide various flight recommendations that match what you
                    want
                  </Text>

                  <View style={styles.exploreWatchContainer}>
                    <Button
                      style={{ flexGrow: 1, paddingVertical: 12 }}
                      title="Explore Now"
                      isFill
                    />
                    <Button
                      style={{ flexGrow: 1 }}
                      title="Watch Demo"
                      color="white"
                      icon={
                        <GetIcon
                          name={icon.play}
                          color="white"
                          size={18}
                          type={isSfOrIon}
                        />
                      }
                    />
                  </View>

                  <View style={styles.storeRatingContainer}>
                    <View style={styles.storeRatingRow}>
                      <Image
                        style={{ height: 40, width: 120 }}
                        source={AppImages.playStoreBadge}
                        resizeMode="contain"
                      />
                      <Text style={styles.storeRatingValue}>4.9</Text>
                      <View style={{ gap: 4 }}>
                        {ratingView()}
                        <Text style={{ fontSize: 9, color: 'darkgrey' }}>
                          990k+ Reviews
                        </Text>
                      </View>
                    </View>
                    <View style={styles.storeRatingRow}>
                      <Image
                        style={{ height: 40, width: 120 }}
                        source={AppImages.appStoreBadge}
                        resizeMode="contain"
                      />
                      <Text style={styles.storeRatingValue}>4.8</Text>
                      <View style={{ gap: 4 }}>
                        {ratingView()}
                        <Text style={{ fontSize: 9, color: 'darkgrey' }}>
                          855k+ Reviews
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
                <View
                  style={[
                    styles.mobileScreenshotContainer,
                    { display: screenWidth > 768 ? 'flex' : 'none' },
                  ]}
                >
                  <Image
                    style={styles.mobileScreenShot}
                    resizeMode="contain"
                    source={AppImages.travel_req_half_frame}
                  />
                </View>
              </View>
            </View>
          </ImageBackground>

          <View
            style={[
              styles.secondContentView,
              safeAreaStyle(insets, 24).paddingH,
            ]}
          >
            <Text style={styles.appDescText}>
              We are here to provide a feature which will make it easier for you
              to make{' '}
              <Text style={{ color: 'rgb(82, 176, 167)' }}>
                flight bookings, refunds, round trip, recommendations and many
                other
              </Text>{' '}
              interesting features for you.{' '}
              <Image
                style={styles.appDescFlightIcon}
                source={AppImages.flight_icon}
              />
            </Text>
            <Image style={styles.downArrowIcon} source={AppImages.down_arrow} />

            <Text style={styles.airlineCollabTitle}>
              We Work with more than 600+ airlines worldwide
            </Text>
            <View
              style={[
                styles.airlinesLogoContainer,
                {
                  justifyContent:
                    screenWidth >= 500 ? 'space-between' : 'space-around',
                },
              ]}
            >
              {COLLAB_AIRLINES.map((logo, i) => (
                <Image
                  key={`air_${i}`}
                  style={styles.airlinesLogo}
                  source={logo}
                  resizeMode="contain"
                  // @ts-expect-error: No Property
                  tintColor="darkgrey"
                />
              ))}
            </View>
          </View>

          <View
            style={[
              styles.thirdSectionContainer,
              safeAreaStyle(insets).paddingH,
            ]}
          >
            <AppPreviewCard {...{ scrollOffset }} />
            <View style={styles.accordianContainer}>
              <Text style={{ fontSize: 32, fontWeight: 'bold' }}>
                See what we do
              </Text>
              <Text style={{ marginVertical: 16 }}>
                Ngefly provides the best features for your convenience when
                traveling long distances
              </Text>
              <FeatureAccordian />
            </View>
          </View>

          <View
            style={[
              styles.fourthSectionContainer,
              safeAreaStyle(insets, 16).paddingH,
            ]}
          >
            <Image
              style={styles.fourthSectionEmojiImg}
              source={AppImages.flight_icon}
            />
            <View style={styles.fourthSectionTitleContainer}>
              <Text style={styles.fourthSectionTitle}>
                Let's discover your destination
              </Text>
              <Text style={styles.fourthSectionSubtitle}>
                We help you to find flights easily and efficiently. we provide
                various flight recommendations that match what you want
              </Text>
            </View>
            <Image
              style={styles.fourthSectionEmojiImg}
              source={AppImages.map_icon}
            />
          </View>
          <View
            onLayout={event =>
              (travelInputsPosY.current = event.nativeEvent.layout.y)
            }
          >
            <View style={{ ...StyleSheet.absoluteFillObject }}>
              <View style={{ flex: 1 }} />
              <View
                style={{ flex: 1, backgroundColor: 'rgb(245, 247, 250)' }}
              />
            </View>
            <View style={[{ margin: 24 }, safeAreaStyle(insets, 24).marginH]}>
              {screenWidth <= 600 ? (
                <TravelDetails isFullTabView />
              ) : (
                <TravelDetailsDesktop />
              )}
            </View>
          </View>
          <View
            style={[
              { backgroundColor: 'rgb(245, 247, 250)', padding: 16 },
              safeAreaStyle(insets, 16).paddingH,
            ]}
          >
            <View>
              {flightsList.data.result.map((item, index) =>
                index < 3 ? (
                  <View key={`${item.id}_${index}`}>
                    {screenWidth <= 600 ? (
                      <FlightItem info={item} onClick={() => {}} />
                    ) : (
                      <FlightItemDesktop info={item} onClick={() => {}} />
                    )}
                  </View>
                ) : null,
              )}
            </View>
            <View style={{ alignItems: 'center', padding: 24 }}>
              <Button
                style={{ backgroundColor: 'white' }}
                textStyle={{ color: 'black', fontWeight: '600' }}
                title="Show All"
                isFill
                color="black"
                onPress={() => {
                  const dep = travelState.departure.trim().length === 0;
                  const dest = travelState.destination.trim().length === 0;
                  if (dep || dest) {
                    scrollRef.current?.scrollTo({
                      y: travelInputsPosY.current,
                      animated: true,
                    });
                  } else {
                    navigation.navigate('flights_list');
                  }
                }}
              />
            </View>
          </View>
          <ImageBackground
            style={styles.headerContainer}
            // @ts-expect-error
            imageStyle={pStyles.headerImageBg}
            source={AppImages.dotted_world_map}
            resizeMode="contain"
          >
            <View
              style={[
                styles.headerContentContainer,
                safeAreaStyle(insets).paddingH,
              ]}
            >
              <View style={styles.sixthSectionContainer}>
                <View style={styles.sixthSecTitleContainer}>
                  <Text style={styles.sixthSecTitle}>
                    Let's find your next flight
                  </Text>
                  <View
                    style={{ alignItems: 'flex-start', marginVertical: 16 }}
                  >
                    <Button
                      style={{ paddingVertical: 12 }}
                      title="Get Started"
                      isFill
                    />
                  </View>
                </View>
                <View style={{ minWidth: '30%', maxHeight: 350 }}>
                  <Image
                    style={styles.sixthSecAppPreviewImg}
                    source={AppImages.ngefly_preview}
                    resizeMode="contain"
                  />
                </View>
              </View>
            </View>
          </ImageBackground>
          <View
            style={[
              styles.socialsContainer,
              safeAreaStyle(insets, 16).paddingH,
            ]}
          >
            <Text style={{ fontSize: 16, fontWeight: '500' }}>Socials</Text>
            <View style={{ flexDirection: 'row', gap: 16 }}>
              {SOCIALS.map(({ iconName, color, url }) =>
                socialLinks(iconName, color, url),
              )}
            </View>
          </View>
          <Text
            style={[
              styles.copyright,
              { paddingBottom: insets.bottom + 16 },
              safeAreaStyle(insets, 16).paddingH,
            ]}
          >
            © Copyright 2023 NgeFly. All Rights Reserved
          </Text>
        </ScrollView>
      </KeyboardAvoidingView>
      <MenuModal {...{ toggleMenu }} onClose={() => setToggleMenu(false)} />
    </>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    width: '100%',
    backgroundColor: 'rgb(23, 27, 34)',
    overflow: 'hidden',
  },
  headerContentContainer: {
    backgroundColor: 'rgba(23, 27, 34, 0.6)',
    zIndex: 1000,
  },
  headerContentView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding: 24,
    paddingBottom: 0,
  },
  headerTitle: {
    maxWidth: 320,
    fontSize: 50,
    fontWeight: 'bold',
    color: 'white',
    lineHeight: 60,
  },
  headerSubtitle: {
    maxWidth: 320,
    color: 'rgba(255, 255, 255, 0.5)',
    paddingVertical: 16,
    lineHeight: 24,
  },
  exploreWatchContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    alignSelf: 'flex-start',
    paddingVertical: 16,
    paddingEnd: 8,
  },
  storeRatingContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: 'rgb(40, 46, 61)',
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    padding: 16,
    marginTop: 36,
    gap: 8,
    columnGap: 40,
  },
  storeRatingRow: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 4,
  },
  storeRatingValue: {
    fontSize: 24,
    color: 'white',
    marginHorizontal: 8,
  },
  mobileScreenshotContainer: {
    justifyContent: 'flex-end',
    marginEnd: 16,
  },
  mobileScreenShot: {
    width: 300,
    height: 300 / (1300 / 1740),
    overflow: 'visible',

    // ...Platform.select({
    //   default: {
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.8,
    shadowRadius: 24,
    //   },
    //   web: { boxShadow: '0px 0px 24px rgba(0, 0, 0, 0.8)' },
    // }),
  },
  // Second Section
  secondContentView: {
    backgroundColor: 'rgb(245, 247, 250)',
    paddingVertical: 40,
  },
  appDescText: {
    fontSize: 24,
    fontWeight: '600',
    lineHeight: 30,
  },
  appDescFlightIcon: {
    width: 25,
    height: 25,
    shadowColor: 'rgb(102, 183, 232)',
    shadowOffset: { width: 0, height: 16 },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    overflow: 'visible',
  },
  downArrowIcon: {
    width: 25.5,
    height: 60,
    alignSelf: 'center',
    marginVertical: 24,
  },
  airlineCollabTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  airlinesLogoContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    paddingVertical: 8,
    gap: 8,
  },
  airlinesLogo: {
    width: 150,
    height: 44,
    ...Platform.select({
      default: { tintColor: 'darkgrey' },
      web: {},
    }),
  },
  // third Section
  thirdSectionContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    paddingVertical: 24,
  },
  accordianContainer: {
    minWidth: '50%',
    flexGrow: 1,
    flexShrink: 1,
    margin: 24,
  },
  // Fourth Section
  fourthSectionContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 16,
  },
  fourthSectionEmojiImg: {
    width: 50,
    height: 50,
    margin: 16,
    marginHorizontal: 24,
    shadowColor: 'rgb(102, 183, 232)',
    shadowOffset: { width: 0, height: 32 },
    shadowOpacity: 0.2,
    shadowRadius: 4.65,
    overflow: 'visible',
  },
  fourthSectionTitleContainer: {
    flexGrow: 1,
    flexShrink: 1,
    minWidth: '30%',
  },
  fourthSectionTitle: {
    fontSize: 32,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  fourthSectionSubtitle: {
    fontSize: 16,
    color: 'grey',
    textAlign: 'center',
    marginTop: 16,
  },
  // Sixth sections
  sixthSectionContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap-reverse',
    justifyContent: 'space-around',
    padding: 24,
    gap: 32,
  },
  sixthSecTitleContainer: {
    flexGrow: 1,
    flexShrink: 1,
    minWidth: '30%',
    rowGap: 24,
  },
  sixthSecTitle: {
    maxWidth: 400,
    fontSize: 60,
    fontWeight: 'bold',
    color: 'white',
  },
  sixthSecAppPreviewImg: {
    width: '100%',
    maxHeight: 350,
    minHeight: 150,
    aspectRatio: 1,
  },
  socialsContainer: {
    padding: 16,
    backgroundColor: 'rgb(245, 247, 250)',
    gap: 8,
  },
  copyright: {
    color: 'rgb(23, 27, 34)',
    padding: 16,
    textAlign: 'center',
  },
});

// Short for Platform Styles, seperated to avoid multiple lint error cause of web specific styles
const pStyles = StyleSheet.create({
  // @ts-expect-error: Incompatible properties
  headerImageBg: {
    ...Platform.select({
      default: {
        transform: [
          { perspective: 1000 },
          { rotateX: '40deg' },
          { scaleX: 1.2 },
        ],
      },
      web: {
        transform: 'perspective(1000px) rotateX(40deg) scaleX(1.2)',
      },
    }),
  },
});

const safeAreaStyle = (insets: EdgeInsets, plus: number = 0) =>
  StyleSheet.create({
    paddingH: {
      paddingLeft: insets.left + plus,
      paddingRight: insets.right + plus,
    },
    marginH: {
      marginLeft: insets.left + plus,
      marginRight: insets.right + plus,
    },
  });

export default LandingScene;
