import React, { useEffect, useRef, useState } from 'react';
import {
  Animated,
  GestureResponderEvent,
  LayoutChangeEvent,
  Pressable,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import IonIcons from 'react-native-vector-icons/Ionicons';
import { Config } from '../../../config';
import { ACCORDIAN_LIST } from '../../../model/data';

interface AccordianProps {
  item: (typeof ACCORDIAN_LIST)[0];
  isOpen: boolean;
  onPress: ((event: GestureResponderEvent) => void) | null | undefined;
}

const AnimatedIonIcons = Animated.createAnimatedComponent(IonIcons);

const AccordingItem: React.FC<AccordianProps> = ({ item, isOpen, onPress }) => {
  const layoutHeight = useRef({ container: 0, text: 0 });
  const accHeight = useRef<Animated.Value | undefined>(undefined);
  const iconRotation = useRef<Animated.Value>(new Animated.Value(0));

  useEffect(() => {
    if (accHeight.current === undefined) {
      accHeight.current = new Animated.Value(0);
      return;
    }

    const expandedHeight =
      layoutHeight.current.container + layoutHeight.current.text;

    Animated.parallel([
      Animated.spring(accHeight.current, {
        toValue: isOpen ? expandedHeight : layoutHeight.current.container,
        useNativeDriver: false,
      }),
      Animated.spring(iconRotation.current, {
        toValue: isOpen ? 180 : 0,
        useNativeDriver: false,
      }),
    ]).start();
  }, [isOpen]);

  const roation = iconRotation.current.interpolate({
    inputRange: [0, 180],
    outputRange: ['0deg', '180deg'],
  });

  const calculateLayout = (e: LayoutChangeEvent) => {
    // layout is calculated here instead at parent for responsiveness on web
    // but it returns 0 second time on iOS for some reason (not on web)
    if (Config.isWeb || layoutHeight.current.container === 0) {
      let layoutH = e.nativeEvent.layout.height;

      const newHeight = layoutH + 32;
      layoutHeight.current.container = newHeight;

      accHeight.current?.setValue(
        newHeight + (isOpen ? layoutHeight.current.text : 0),
      );
    }
  };

  return (
    <Animated.View
      style={[
        {
          height: accHeight.current,
          backgroundColor: isOpen ? 'rgb(245, 247, 250)' : undefined,
          borderColor: !isOpen ? 'lightgrey' : 'transparent',
        },
        styles.itemContainer,
      ]}
    >
      <Pressable
        style={({ pressed }) => [
          { flexDirection: 'row', opacity: pressed ? 0.6 : 1 },
        ]}
        onLayout={calculateLayout}
        onPress={onPress}
      >
        <Text
          style={{ flex: 1, fontSize: 20, fontWeight: '600' }}
          numberOfLines={1}
        >
          {item.title}
        </Text>
        <AnimatedIonIcons
          style={{ transform: [{ rotate: roation }] }}
          name="ios-chevron-down"
          size={20}
          color="grey"
        />
      </Pressable>
      <View>
        <Text
          style={{ position: 'absolute', color: 'grey', paddingTop: 16 }}
          onLayout={e => {
            // if (layoutHeight.current.text === 0) {
            layoutHeight.current.text = e.nativeEvent.layout.height;
            // }
          }}
        >
          {item.message}
        </Text>
      </View>
    </Animated.View>
  );
};

const FeatureAccordian = () => {
  const [activeIndex, setActiveIndex] = useState(-1);

  return (
    <View>
      {ACCORDIAN_LIST.map((item, index) => (
        <AccordingItem
          key={`${item.title}_${index}`}
          item={item}
          isOpen={activeIndex === index}
          onPress={() => setActiveIndex(activeIndex === index ? -1 : index)}
        />
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  itemContainer: {
    borderWidth: 1,
    borderRadius: 8,
    padding: 16,
    marginVertical: 8,
    overflow: 'hidden',
  },
});

export default FeatureAccordian;
