import React, { useRef, useState } from 'react';
import { Animated, Pressable } from 'react-native';

const INNER_PADDING = 4;
const DEFAULT_WIDTH = 60;
const DEFAULT_HEIGHT = 28;
const CIRCLE_SIZE = DEFAULT_HEIGHT - 2 * INNER_PADDING; // 20

const SIZE_WITH_PADDING = DEFAULT_HEIGHT;
const MAX_TRANSLATE = DEFAULT_WIDTH - SIZE_WITH_PADDING; // 32

const AnimatedPressable = Animated.createAnimatedComponent(Pressable);

const Switch = () => {
  const [isChecked, setChecked] = useState(false);

  const translateCircle = useRef(new Animated.Value(0));
  const circleWidth = useRef(new Animated.Value(CIRCLE_SIZE));

  // const panResponder = React.useRef(
  //   PanResponder.create({
  //     onStartShouldSetPanResponder: (evt, gestureState) => true,
  //     // onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
  //     onMoveShouldSetPanResponder: (evt, gestureState) => true,
  //     // onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
  //     onPanResponderGrant: (evt, gestureState) => {},
  //     onPanResponderMove: (evt, gestureState) => {
  //       const touchLocation = evt.nativeEvent.locationX;

  //       /* if (touchLocation >= 14 && touchLocation <= 46) {
  //         translateCircle.current.setValue(touchLocation - 14);
  //       } else {
  //         translateCircle.current.setValue(
  //           Math.max(0, Math.min(touchLocation - 14, 32)),
  //         );
  //       } */
  //       updateOnTouchMove(evt);
  //     },
  //     onPanResponderRelease: (evt, gestureState) => {
  //       // const touchLocation = evt.nativeEvent.locationX;
  //       // onValueChange(touchLocation > 30);
  //     },
  //   }),
  // ).current;

  // const updateOnTouchMove = (e: GestureResponderEvent) => {
  //   const touchLocation = e.nativeEvent.locationX;
  //   if (
  //     (isChecked && touchLocation <= 14) ||
  //     (!isChecked && touchLocation >= 46)
  //   ) {
  //     activatePressAnim(false);
  //   }
  // };

  const activatePressAnim = (active: boolean) => {
    let translateX = 0;
    if (active) {
      translateX = isChecked ? MAX_TRANSLATE - INNER_PADDING : 0;
    } else {
      translateX = !isChecked ? MAX_TRANSLATE : 0;
    }

    Animated.parallel([
      Animated.spring(translateCircle.current, {
        toValue: translateX,
        // Color interpolate not working on iOS when setting it to true
        useNativeDriver: false,
      }),
      Animated.spring(circleWidth.current, {
        toValue: active ? 24 : CIRCLE_SIZE,
        useNativeDriver: false,
      }),
    ]).start();

    if (!active) {
      setChecked(!isChecked);
    }
  };

  const colorAnim = translateCircle.current.interpolate({
    inputRange: [0, MAX_TRANSLATE],
    outputRange: ['rgb(231, 236, 243)', 'rgb(82, 176, 167)'],
    extrapolate: 'clamp',
  });

  return (
    <AnimatedPressable
      style={{
        width: DEFAULT_WIDTH,
        height: DEFAULT_HEIGHT,
        padding: INNER_PADDING,
        borderRadius: 16,
        backgroundColor: colorAnim,
      }}
      //   onPress={() => onValueChange(!isChecked)}
      onPressIn={() => activatePressAnim(true)}
      onPressOut={() => activatePressAnim(false)}
      // onPressOut={updateOnTouchMove}
      // onTouchMove={updateOnTouchMove}
    >
      <Animated.View
        style={{
          width: circleWidth.current,
          height: CIRCLE_SIZE,
          backgroundColor: 'white',
          borderRadius: 999,
          transform: [{ translateX: translateCircle.current }],
        }}
      />
    </AnimatedPressable>
  );
};

export default Switch;
