import { Config } from '../config';

export const isSfOrMaterial = Config.isIos ? 'sf' : 'material';
export const isSfOrIon = Config.isIos ? 'sf' : 'ion';

export const icon = {
  plane_takeoff: Config.isIos ? 'airplane.departure' : 'flight-takeoff',
  plane_land: Config.isIos ? 'airplane.arrival' : 'flight-land',
  calendar: Config.isIos ? 'calendar' : 'ios-calendar-outline',
  person_circle: Config.isIos ? 'person.crop.circle' : 'person-circle-outline',
  search: Config.isIos ? 'magnifyingglass' : 'ios-search-outline',
  arrow_double: Config.isIos ? 'chevron.forward.2' : 'double-arrow',
  arrow_round: Config.isIos ? 'arrow.triangle.2.circlepath' : 'ios-sync',
  wallet: Config.isIos ? 'wallet.pass' : 'wallet-outline',
  globe: Config.isIos ? 'globe' : 'ios-globe-outline',
  play: Config.isIos ? 'play' : 'play-outline',
  menu: Config.isIos ? 'line.3.horizontal' : 'menu',
  close: Config.isIos ? 'xmark' : 'close',
  arrow_back: Config.isIos ? 'chevron.backward' : 'arrow-back-ios',
  arrow_forward: Config.isIos ? 'arrow.forward' : 'arrow-forward',
};
