## RN Travel App

UI Inspiration: [Dribbble Ngefly](https://dribbble.com/shots/20298972-Flight-Boking-App-Ngefly)

### 🖥 Running Locally

This project is built on react-native CLI, so make sure you've [set up development environment](https://reactnative.dev/docs/environment-setup).

```bash
# clone the project and cd into it
git clone https://gitlab.com/ashud/rn_travel_app.git
cd ./rn_travel_app

# install dependencies
yarn install

# iOS only
npx pod-install ios
# or
cd ios && pod install && cd ..

# Run iOS
npx react-native run-ios

# Run Android
npx react-native run-android

```

#### 🌐 Running on Web

```bash
# From project's root, run
yarn web

# Logs will mention where the project is running, for ex. 'http://localhost:8080/'
# then open that localhost url on any web browser.
```

#### 🍎🖥 Running on macOS

[react-native-macos](https://github.com/microsoft/react-native-macos) doesn't support navigation from [@react-navigation/native-stack](https://reactnavigation.org/docs/native-stack-navigator) as that contains native code, so to enable navigation [@react-navigation/stack](https://reactnavigation.org/docs/stack-navigator/) needs to be used, which is javascript based navigation implementation.

So to make that work you also need to install [react-native-gesture-handler](https://docs.swmansion.com/react-native-gesture-handler/docs/installation) with required setup, which is currently removed as it caused issues on Web (latest version).

After this, follow below steps to run the app

```bash
// Install pods
cd macos && pod install && cd ..

// run the project as macos application
npx react-native run-macos
```

### 🪟🖥 Running on Windows

See the react-native-windows [guide](https://microsoft.github.io/react-native-windows/docs/getting-started) for details about required development setup

```sh
npx react-native run-windows
```

### Packages Used

1. [@react-navigation](https://reactnavigation.org):- for in-app navigations
2. [redux](http://redux.js.org): for state management, mainly for travel info being used in multiple components
3. [react-native-vector-icons](https://github.com/oblador/react-native-vector-icons):- icons for some of the actions or visuals like flight, filter, calenar etc.
4. [react-native-safe-area-context](https://github.com/th3rdwave/react-native-safe-area-context):- to handle safe area spacing for some parts of the UI
5. [@react-native-community/datetimepicker](https://github.com/react-native-community/datetimepicker):- to show date picker to select travel date

### Testing

App contains test cases for some of the scenarios.
Use following commands to run the tests

```sh
yarn test
```

### Screenshots

| Initial Screen | After flight selection |
| :---: | :---: |
| <img alt="Initial Screen" src="./readme/initial_screen.png" height="300px"> | <img alt="Initial Screen after flight selection" src="./readme/initial_screen_after_selection.png" height="300px"> |

| Flights List | Flight Filter Modal |
| :---: | :---: |
| <img alt="Flights List" src="./readme/flights_list.png" height="300px"> | <img alt="Flight Filter Modal" src="./readme/filter_modal.png" height="300px"> |
