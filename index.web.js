import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';

// Use prebuilt version of RNVI in dist folder
// import Icon from 'react-native-vector-icons/dist/MaterialIcons';

// Generate required css
import materialIconFont from 'react-native-vector-icons/Fonts/MaterialIcons.ttf';
import IoniconsFont from 'react-native-vector-icons/Fonts/Ionicons.ttf';

const iconFontStyles = `@font-face {
  font-family: MaterialIcons;
  src: url(${materialIconFont});
}
@font-face {
  font-family: Ionicons;
  src: url(${IoniconsFont});
}`;

// Create stylesheet
const style = document.createElement('style');
style.type = 'text/css';
if (style.styleSheet) {
  style.styleSheet.cssText = iconFontStyles;
} else {
  style.appendChild(document.createTextNode(iconFontStyles));
}

// Inject stylesheet
document.head.appendChild(style);

if (module.hot) {
  module.hot.accept();
}
AppRegistry.registerComponent(appName, () => App);
AppRegistry.runApplication(appName, {
  initialProps: {},
  rootTag: document.getElementById('root'),
});
