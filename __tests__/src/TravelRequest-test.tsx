// import 'react-native';
import React from 'react';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import TravelRequest from '../../src/screens/TravelRequest';
import configureStore from '../../src/store';
import * as Route from '../../src/Routes';

// Note: test renderer must be required after react-native.
// import renderer from 'react-test-renderer';
import {
  render,
  fireEvent,
  isHiddenFromAccessibility,
} from '@testing-library/react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Text, View } from 'react-native';
// import { SafeAreaProvider } from 'react-native-safe-area-context';

/* it('renders correctly', () => {
  renderer.create(<TravelRequest />);
}); */

jest.mock('react-native-vector-icons/MaterialIcons', () => 'MaterialIcons');
jest.mock('react-native-vector-icons/Ionicons', () => 'Ionicons');
// jest.mock('react-native-safe-area-context', () => mockSafeAreaContext);
jest.mock('react-native-safe-area-context', () => {
  const inset = { top: 0, right: 0, bottom: 0, left: 0 };
  return {
    ...jest.requireActual('react-native-safe-area-context'),
    SafeAreaProvider: jest.fn(({ children }) => children),
    SafeAreaConsumer: jest.fn(({ children }) => children(inset)),
    useSafeAreaInsets: jest.fn(() => inset),
    useSafeAreaFrame: jest.fn(() => ({ x: 0, y: 0, width: 320, height: 640 })),
  };
});

describe('Testing some TravelRequest behaviours', () => {
  test('input swap properly', async () => {
    const travelComp = render(
      <Provider store={configureStore}>
        {/* <SafeAreaProvider> */}
        <NavigationContainer>
          <TravelRequest />
        </NavigationContainer>
        {/* </SafeAreaProvider> */}
      </Provider>,
    );

    const departure = travelComp.getByLabelText('Departure');
    const destination = travelComp.getByLabelText('Destination');

    fireEvent.changeText(departure, 'Jaipur');
    fireEvent.changeText(destination, 'Gurgaon');

    fireEvent.press(travelComp.getByLabelText('swapBtn'));
    // fireEvent.press(travelComp.getByRole('button', { name: 'swap-vert' }));

    expect(travelComp.queryByDisplayValue('Gurgaon')).toBe(departure);
    expect(travelComp.queryByDisplayValue('Jaipur')).toBe(destination);
    // expect(destination).toHaveTextContent('Jaipur');
  });

  test('clicking on search button shouldnt navigate if inputs are empty', async () => {
    const travelComp = renderNavigator(TravelRequest);

    const toClick = travelComp.getByLabelText('searchButton');
    const departure = travelComp.getByLabelText('Departure');
    const destination = travelComp.getByLabelText('Destination');

    fireEvent.press(toClick);

    expect(departure).toBeOnTheScreen();
    expect(destination).toBeOnTheScreen();
  });

  test('clicking on search button should navigate if inputs are valid', async () => {
    const travelComp = renderNavigator(TravelRequest);

    const toClick = travelComp.getByLabelText('searchButton');
    const departure = travelComp.getByLabelText('Departure');
    const destination = travelComp.getByLabelText('Destination');

    fireEvent.press(toClick);

    expect(isHiddenFromAccessibility(departure)).toBe(true);
    expect(isHiddenFromAccessibility(destination)).toBe(true);
  });
});

function renderNavigator(component: () => JSX.Element) {
  const Stack = createNativeStackNavigator();

  const TestScreen = () => (
    <View>
      <Text>Flights List</Text>
    </View>
  );

  return render(
    <Provider store={configureStore}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name={Route.TRAVEL_REQUEST} component={component} />
          <Stack.Screen name={Route.FLIGHTS_LIST} component={TestScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>,
  );
}
