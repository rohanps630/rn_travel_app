/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import AppNavigator from './src/AppNavigator';
import { Provider } from 'react-redux';
import store from './src/store';
import { SafeAreaProvider } from 'react-native-safe-area-context';
// import { GestureHandlerRootView } from 'react-native-gesture-handler';

function App(): JSX.Element {
  return (
    <Provider store={store}>
      <SafeAreaProvider>
        {/* <GestureHandlerRootView style={{ flex: 1 }}> */}
        <AppNavigator />
        {/* </GestureHandlerRootView> */}
      </SafeAreaProvider>
    </Provider>
  );
}

export default App;
